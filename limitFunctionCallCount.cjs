function limitFunctionCallCount(cb, n = 1) {
  function callBack() {
    if (n > 0) {
      n -= 1;
      return cb();
    } else {
      return null;
    }
  }
  return callBack;
}

module.exports = limitFunctionCallCount;
