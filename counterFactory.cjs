function counterFactory() {
    let value = 0;
    let counterObject = {
      increment: function () {
        value += 1;
        return value;
      },
      decrement: function () {
        value -= 1;
        return value;
      },
    };
    return counterObject;
  }
  
  module.exports = counterFactory;
  