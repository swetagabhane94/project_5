function cacheFunction(cb) {
  {
    let cache = {};
    function callBackFunc(value) {
      if (cache.hasOwnProperty(value)) {
        cache[value] = cb(value);
        return cache[value];
      } else {
        let cacheResult = cb(value);
        //console.log(cache);
        cache[value] = cacheResult;
        return cacheResult;
      }
    }
  }
  return callBackFunc;
}

module.exports = cacheFunction;
