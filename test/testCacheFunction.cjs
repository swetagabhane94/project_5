const cacheFunction = require("../cacheFunction.cjs");

const squareNumber = (number) => {
  return number * number;
};
let test = cacheFunction(squareNumber);

console.log(test(6));
console.log(test(2));
console.log(test(3));

// const operations = () => {
//     return "My code is working";
//   };

//   let test = cacheFunction(operations)
//   console.log(test(0))
//   console.log(test(1))
//   console.log(test(2))
